



var trackSelector = '.d-track';
var trackNameEl = '.d-track__name';
var trackContext = '.d-context-menu';

let upd = function () { 
    setTimeout(() => { 
        var allTrackDivs = document.querySelectorAll(trackNameEl);

        var removeTracks = [];

        allTrackDivs.forEach(trackDiv => {
            var trackTitle = trackDiv.getAttribute("title");
            trackDiv.closest(trackSelector).style.backgroundColor = '#'+intToRGB(hashCode(trackTitle));
            trackDiv.querySelector(".d-track__title").style.color = '#'+intToRGB(-hashCode(trackTitle));
        }); 
        upd();
    }, 300);
};

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToRGB(i){
    var c = (i & 0x00FFFFFF)
        .toString(16)
        .toUpperCase();

    return "00000".substring(0, 6 - c.length) + c;
}


upd();
